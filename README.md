# Databases

Project containing all exercises and lecture notes for the Accelerator Databases course.

The exercise instructions and model answers can be found at this link: https://gitlab.com/thgaccelerator/Databases/tree/master/Excersices