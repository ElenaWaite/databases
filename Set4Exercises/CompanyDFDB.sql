insert into employees values ('1123456789', 'Ross', 'Geller', '20000', '1234567891234', '5' );
insert into employees values ('1223456789', 'Rachel', 'Green', '60000', '1234567892345', '4');
insert into employees values ('1233456789', 'Monica', 'Bing', '75000', '1234567893456', '1');
insert into employees values ('1234456789', 'Chandler', 'Bing', '80000', '1234567894567', '2');
insert into employees values ('1234556789', 'Phoebe', 'Buffet', '65000', '1234567895678', '2');
insert into employees values ('1234566789', 'Joey', 'Tribbiani', '45000', '1234567896789', '3');

insert into dependents values('Ben', '8', '1123456789' );
insert into dependents values('Emma', '2', '1223456789' );
insert into dependents values('Erica', '0', '1233456789');
insert into dependents values('Jack', '0', '1234456789');
insert into dependents values('Frank jnr jnr', '5', '1234556789');
insert into dependents values('Leslie', '5', '1234556789');

insert into departments values('1', 'Restaurant', '100000', '1233456789');
insert into departments values('2', 'Transponsters', '500', '1234456789');
insert into departments values('3', 'Acting', '200000', '1234566789');
insert into departments values('4', 'Fashion', '3000000', '1223456789');
insert into departments values('5', 'Dinosaurs', '40', '1123456789');


