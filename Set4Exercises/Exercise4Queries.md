#Company database queries:
1. Count the number of distinct queries in the database
SELECT DISTINCT salary FROM employees

2. Find the names and average salaries of all departments whose average salary is greater than 42000
SELECT dnum, AVG(salary)
FROM employee
GROUP BY dnum
HAVING AVG(salary) > 42000;

3. For each department retrieve the department number, the number of employees in the department, and their average salary (Company DB)
SELECT department.dnum, COUNT(employee.NI), AVG(employee.salary)
FROM department, employee
GROUP BY dnum

4. For each project, retrieve the project number, the project name, and the number of employees who work on that project
SELECT project.pnum, project.pname, COUNT(works_on.pno)
FROM project, works_on
GROUP BY pnum;

5. For each project on which more than two employees work, retrieve the project number, the project name, and the number of employees who work on the project
SELECT project.pnum, project.pname, COUNT(works_on.pno)
FROM project, works_on
GROUP BY pnum;
HAVING COUNT(works_on.pno) > 2;

6. Find the sum of the salaries of all employees of the 'Research' department, as well as the maximum salary, the minimum salary, and the average salary in this department
SELECT MAX(employee.salary), MIN(employee.salary), AVG(employee.salary), SUM(employee.salary)
FROM employee, department
WHERE employee.dno = department.dnum AND department.dname = 'Research';

7. Retrieve the total number of employees in the company and the number of employees in the 'Research' department
SELECT COUNT(employee.NI), COUNT(works_on.eNI)
FROM employee, works_on, department
WHERE employee.dno = department.dnum AND department.dname = 'Research';

8. Retrieve the national insurance numbers of all employees who work on project number 1, 2, or 3
SELECT employee.NI
FROM employee, works_on
WHERE employee.NI = works_on.eNI AND works_on.eNI = '1' or '2' or '3';

9. For each department having more than five employees, retrieve the department number and the number of employees making more than £40,000
SELECT department.dnum, COUNT(works_on.eNI)
FROM department, employee, works_on
WHERE COUNT(works_on.eNI ) > 5 AND works_on.eNI = employee.NI AND employee.salary > 40000;