#import database 
import sqlite3

from employee import Employee

#create variable, with where to store database and import/create
# employee.db is created 
#conn = sqlite3.connect('employee.db')

# can set the above equal to memory
conn = sqlite3.connect(':memory:')

# creating a cursor
c = conn.cursor()

#once created, comment it out, don't need to make again
c.execute("""CREATE TABLE employees (
             first text,
           last text,
            pay integer
)""")

def insert_emp(emp):
    # in context manager as needs to be committed
    with conn:
        # second proper way to use placeholders in dict form
        c.execute("INSERT INTO employees VALUES (:first, :last, :pay)", {'first': emp.first, 'last': emp.last, 'pay': emp.pay})

def get_emps_by_name(lastname):
    # for dict insert statement
    # doesn't need to be comitted so not in a ocntext manager like our inserts
    c.execute("SELECT * FROM employees WHERE last=:last", {'last': lastname})
    return c.fetchall()

def update_pay(emp, pay):
    with conn:
        c.execute("""UPDATE employees SET pay = :pay
        WHERE first = :first AND last = :last""",
        {'first': emp.first, 'last': emp.last, 'pay': pay})

def remove_emp(emp):
    with conn:
        c.execute("DELETE from employees WHERE first = :first AND :last",
        {'first': emp.first, 'last': emp.last})

emp_1 = Employee('Molly', 'Weasley', 45000)
emp_2 = Employee('Arthur', 'Weasley', 55000)

#setting the data for data that you already have into the columns
#this is not best practice
#c.execute("INSERT INTO employees VALUES ('{}', '{}', {})".format(emp_1.first, emp_1.last, emp_1.pay))

#this is with the question marks
#c.execute("INSERT INTO employees VALUES (?, ?, ?)", (emp_1.first, emp_1.last, emp_1.pay) )


#once run, comment out 
# c.execute("INSERT INTO employees VALUES ('Remus', 'Lupin', 100000)")

# for question mark insert statement
# comma is needed as it needs to be a Tuple
# c.execute("SELECT * FROM employees WHERE last= ? ", ('Waite',))

insert_emp(emp_1)
insert_emp(emp_2)

emps = get_emps_by_name('Weasley')
print (emps)

update_pay(emp_2, 95000)
remove_emp(emp_1)

emps = get_emps_by_name('Weasley')
print(emps)

conn.close()
