Set 3 Exercise 1

1. Retrieve names of all employees in department 5 who work more than 10 hours a week on the "Product Mega" project
SELECT works_on.eNI
FROM works_on, project
WHERE works_on.hours > 10 AND works_on.pno = project.pnum AND project.pname = 'Product Mega';

2. Show the resulting salaries if every employee working on the 'Product Alfa' project is given a 10% raise
SELECT employee.salary * 1.1 
FROM employee, project 
WHERE project.pname = 'Project Alfa';

3. List the names of all employees who have a dependent with the same first name as themselves
SELECT employee.fnam
FROM employee, dependent 
WHERE dependent.fname = dependent.dependent_name;

4. Retrieve a list of employees and the projects they are working on, ordered by department, and within each department, alphabetically by last name, first name.
SELECT employee.fnam, employee.lname, project.pname
FROM employee, project
ORDER BY employee.dno, employee.fname ASC, employee.lname ASC;

5. Find the names of all employees who are directly supervised by Franklin Wong
SELECT fname, lname 
FROM employee, department
WHERE superNI = employee.NI 
AND employee.fname = 'Franklin' 
AND employee.lname = 'Wong';

Set 3 Exercise 2 
1. Retrieve the names of all senior students majoring in 'cs' (computer science)
SELECT student.name 
FROM student, takes
WHERE student.ID = takes.ID AND takes.course_id = 'CS' AND takes.year = '3'

2. Retrieve the names of all courses taught by Professor King in 2007 and 2008.
SELECT course.title
FROM teaches, instructor, course
WHERE course.course_id = teaches.course_id AND teaches.ID = instructor.ID AND instructor.name = 'King';

3. For each section taught by Professor King, retrieve the course number, semester, year, and number of students who took the section.

SELECT section.course_ID, section.semester, section.year, COUNT(teaches.ID)
FROM section, instructor, teaches
WHERE section.course_id = takes.course_id AND teaches.ID = instructor.ID AND instructor.name = 'King';

4. Retrieve the name and transcript of each senior student (Class = 4) majoring in CS. A transcript includes course name, course number, credit hours, semester, year and grade for each course completed by the student

SELECT course.course_name, course.course_number, course.credit hours, section.semester, section.year, section.instructor, grade_report.grade 
FROM course, section, grade_report 
WHERE student.student_number = 4

5. For all instructors who have taught some course, find their names and the course ID and name of the courses they taught
SELECT instructor.name, teaches.course_id, course.title 
FROM teaches, course, instructor
WHERE course.course_id = teaches.course_id AND teaches.id = instructor.id

