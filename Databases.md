
## What is a database?
Structured set of data
E.F. Codd - came up with relational databases

## Relational Databases
All data in tuples
Grouped in relations
Declarative - state what you want and the DBMS takes care of structure and queries
SQL

## Database Management Systems
System for creating and managing databases
Create, read, update, delete data
Manage and control the layout of data
Wikipedia - comparison of relational database management systems
Years of thought - want to use someone else\'92s not make your own

## Database Fundamentals
## Relational Database Designs 
Fundamentals of database systems (7th edition) - Elmasri, Ramez and Shamkant B. Navathe
DBMS Component Modules

## Levels of Abstraction
Physical internal level: describes how a record (e.g., customer) is stored
Conceptual logical: describes entities, data types, relationships, user operations, and constraints.
View external level: describes the part of the database that a particular user group is interested in and hides the rest of the database from that user group.
Most DBMSs do not separate the three levels completely and explicitly, but they support this architecture to some extent.

## Traditional Design Methods
Traditional methods of database design incorporate three phases: requirement analysis, data modelling, and normalisation

### Modeling
Provides a means of visually representing various aspects of the database structure, such as the tables, table relationships, and relationship characteristics.

### Data Model
A data model is a notation for describing data or information, it describes:
- the structure of the data
- the constraint on the data
- the operation on the data

#### Important data models:
Entity-Relationship data model - mainly for database design
Object-based data models - object-oriented and object-relational
Semi-structured data model - XML and related standards
Other older models:
Network model
Hierarchical model

## The Entity-Relationship (ER) Model:
Models an enterprise as a collection of entities and relationships
Entity: a thing or object in the enterprise that is distinguishable from other objects
Entities have attributes. Example = people have name and addresses
An entity set is a set of entities of the same type that share the same properties. Example = set of all persons, companies, holidays
Relationship: an association among several entities, e.g. an instructor is an advisor to a student, advisor is the relationship
A binary relationship set: 2 groups of entities that share the same relationship with one another, e.g. a group of students and a group of instructors that advise individual students, most relationship sets in a database system are binary

## Tenary relationships:
Students work on research projects under the guidance of an instructor 
Relationship proj_guide is a ternary relationship between instructor, student and project

### The Entity-Relationship (ER) Model 
Models an enterprise as a collection of entities and relationships:
Entity: a thing or object in the enterprise that is indistinguishable 

### Attributes 
An entity is represented by a set of attributes, that is descriptive possessed bu all members of an entity set
Domain - the set of permitted values for each attribute

#### Attribute types:
Simple and composite attributes
Derived attributes - can be computed from other attributes, example: age, given date_of_birth
Single-valued and multivalued attributes - age is a single valued attribute of a person, children's names could be multivalued

### Null Values
A null represents 
Unknown - missing values are commonly the result of human error
- the Height attribute of a person is listed as NULL
Values which are truly unknown - not known whether the attribute value exists
Specific value you need for a field is as yet undefined
- difference between unknown and not applicable - examples

#### Constraints: Keys 
A super key of an entity set is a set of one or more attributes whose values uniquely determine each entity
A candidate key of an entity set is a minimal super key - 
- ID is candidate key if instructor entity
- course_id is candidate key of course entity 

Although several candidate keys may exist, one of the candidate keys is selected to primary key of a relationship set

## More Schema-based constraints
- Constraints are derived from the rules in the mini world that the database represents.
- Referential integrity constraint and foreign keys (FKs), is specified between two relations.
- Entity integrity constraint states that no primary value key can be NULL
- Referential integrity constraints typically arise from relationships among the entities represented by the relation schemas

##  Ratio and Participation Constraints
The cardinality ratio for a binary relationship specifies the maximum number of relationship instances that an entity can participate in 
For example, department:employee is 1:N, a department can have any number of employees but an employee can have only one department
Possible cardinality ratios for binary relationship types are 1:1, 1:N, N:1, and M:N

## In ER
Total Participation (indicated by double line): every entity in the entity set participates in at least one relationship 
The Company Example
The participation of EMPLOYEE in WORKS_FOR is called total/ existence participation
The participation of EMPLOYEE in the MANAGES relationship type is partial
Partial Participation: some entities may not participate in any relationship in the relationship set

## Weak entity - 
Entity types that do not have key attributes of their own are called weak entity types
The existence of a weak entity set depends on the existence of a identifying entity set - it must relate to the identifying entity set via a total, one-to-many relationship set from the identifying to the weak entity set.
Identifying relationship depicted using double diamond 
The discriminator of a weak entity set is the set of attributes that distinguishes among all the entities of a weak entity set 
The primary key of a weak entity set is formed by the primary key of the strong entity set on which the weak entity set is existence dependent, plus the weak entity set's discriminator.
Double line - the lower hierarchy is completely dependent in the higher for its existence
Single line - the higher hierarchy does not need to exist for the lower to exist

Normalisation
Normalisation is the process of decomposing data tables into smaller ones in order to eliminate, minimise duplicate data
During the normalisation process, table structures are tested against normal forms and then modified if any of the problems of duplicate data e.t.c. are found
A normal form is a specific set of rules that can be used to test for a particular set of problems.

Modification Anomalies
Deletion anomaly - deleting more than one set of facts

1st Normal Form
Should have a primary key for each table
Each column contains atomic values which can't be divided any further
There is only one column describing the same thing 

2nd Normal Form

3rd Normal Form
Relationships between columns e.g. postcode and city 
Transitive dependence - a columns value relies on another column , don\'92t want this, should only be related to the primary key
New table relating postcode and city
Customerpostalcity becomes a derived key

Mapping Procedure:
3. Map binary 1:1 relationship - three possible approaches:
Foreign key approach (most useful and recommended unless special conditions exist)
Merged relation approach
Cross reference approach

4. Map of binary 1:N relationship
Two possible approaches:
Foreign Key approach
Cross reference approach

5. Map of binary M:N relationship
Cross reference approach
Primary keys as foreign keys from both relations

6. Map of multivalued attributes:
Create new relation for each 

## The Database Language - SQL
Very high level language, say what to do not how to do it 
Avoid a lot of data retrieval, data modification, indexes, constraints and views

Language components - schema definition, data retrieval, data modification, indexes, constraints, views.

### SQL has two forms:
Interactive (dynamic) SQL - get immediate results. Typical way of entering ad hoc on the terminal.
Embedded(dynamic) - embed SQL statements in host application programs.


## Data Manipulation Language (DML):
#### Language for accessing and manipulating the data organised by the appropriate data model.
DML is also know as the query language, provides the ability to query information, and insert, delete and update tuples.
SQL is the most widely used query language.

#### Relations
The result of an SQL query is called a relation
You can query on one relation, more that one relation.
There are also subqueries and correlated subqueries.

### The select Clause 
The select clause selects data from a database.
The data returned is stored in a result table, called the result-set.

##### Syntax - 
SELECT column/columns FROM table
SELECT * FROM table - the star selects all 

SELECT DISTINCT - returns only different values, if a column in a table contains duplicate values it won't return them.

SELECT ALL - makes sure duplicates aren't removed

### The where Clause
Specifies conditions that the result must satisfy 

##### Syntax - 
SELECT column/columns
FROM table
WHERE condition AND/OR another/condition
Comparison results can be combined using the logical connectives and, or and not

### The from Clause
The from clause lists the relations involved in the query 

### Ambiguous Attribute Names and Aliasing
In SQL the same name can be used for two or more attributes as long as the attributes are in different tables.
We must qualify the attribute name with the relation name to prevent ambiguity.
To do that prefix the relation name to the attribute and seperate the two by a period.

### Ordering the Display of Tuples

##### Syntax -
List in alphabetic order the names of all departments:
select distinct dname
from   department
order by dname

### Null Values 

Two reasons for null values - 
Missing value - unknown
Inapplicable - value does not exist
Never put null in a table if you need to do arithmetic operations on it 

#### Testing for Null
Use IS NULL or IS NOT NULL
SELECT COLUMN
FROM mytable
WHERE column IS/IS NOT NULL
AND/OR another_condition AND/OR

# SQL Database - SQL SERVER
### Data Definition Language (DDL):
Used to build and modify the structure of your tables and other objects in the database. When you execute a DDl statement, it takes effect immediately.

#### CREATE DATABASE
CREATE DATABASE databasename;

#### DELETE DATABASE
DROP DATABASE databasename;

#### Creating a full back up of a current database
BACKUP DATABASE databasename
TO DISK = 'filepath';

#### CREATE TABLE
CREATE TABLE <table name> (
    <attribute name1> <data type 1>,
    <attribute name n> <data type n>;
)

#### ALTER TABLE 
ALTER TABLE table_name
ADD column_name datatype;

#### CREATE CONSTRAINTS
CREATE TABLE table_name (
    column1 datatype constraint,
    column2 datatype constraint,
    column3 datatype constraint,
)

#### PRIMARY KEY
CREATE TABLE Persons (
    ID int NOT NULL PRIMARY KEY,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int
);

#### FOREIGN KEY 
CREATE TABLE Orders (
    OrderID int NOT NULL PRIMARY KEY,
    OrderNumber int NOT NULL,
    PersonID int FOREIGN KEY REFERENCES Persons(PersonID)
);


#### GROUP BY statement
The GROUP BY statement is often used with aggregate functions (COUNT, MAX, MIN, SUM, AVG) to group the result-set by one or more columns.

SELECT column_name(s)
FROM table_name
WHERE condition
GROUP BY column_name(s)
ORDER BY column_name(s);

#### distinct ID
Only the ones that have no duplicates

## Join Semantics
Join operatiions take two relations and return as a result another relation
A join operation is a Cartesian product which requires that tuples in the two relations match

#### Creating Natural Joins
The NATURAL JOIN clause is based on all columns in the two tables that have the same name, and relations only one copy of each common column
It selects rows 

#### Why do we combine data?
Normalisation helps reduce each database table to a single meaning or purpose; makes tables easy to update
Joins can stitch the data back together to answer most of the questions we ask the database

### Types of DB Joins
Cross joins return all combinations of rows from each table. No join conditions are required.
Inner joins return rows where the join condition is met/true. In its most common example, where a primary key is being matched by a foreign key.
Outer joins

### Outer Join Types
An extension of the join operation that avoids loss of information


Left outer join -
Right outer join - 
Full outer join - 


Creating joins with the using cause
If several columns have the same names, but the data types do not match, use the using clause to specify the columns for the join 
Use the using to match only one column when more than one column matches
select name, title

from (instructor natural join teaches)
                        join course using(course_id);

Returns a joins teaches and course via course_id

### Using vs. On Clauses
The using clause is used if several clauses share the same name but you don't want to join using all of these common columns
The ON clause is used to join tables where the column names don't match in both tables
The ON clause 

### Beware of natural join
Beware of unrelated attributes with the same name which equated join

### Summary of Joins:

### Implicit and Explicit Join:
can use where to join - implicit
using join is more efficient than where and it is better to be explicit

### Self-Join
In some places we need to join the table to itself

### Semantics 

### Join vs Union
Joins combine data into new columns where as unions combine data into new rows

### Intersect and Inner Join
The INNER JOIN keyword selects records that have matching values in both tables.

SELECT column_name(s)
FROM table1
INNER JOIN table2 ON table1_name = table2.column_name;

### Outer Join
#### Left Outer Join
The Left Join keyword returns all records from the left table and the matched records from the right table.
The result is NULL from the left side if there is no match 

SELECT column_name(s)
FROM table1
LEFT JOIN table2 ON table1.column_name = table2.column_name;

#### Right Outer Join 
The RIGHT JOIN keyword returns all records from the right table (table2), and the matched records from the left table (table1). The result is NULL from the left side, when there is no match.

SELECT column_name(s)
FROM table1
RIGHT JOIN table2 ON table1.column_name = table2.column_name;

#### Full Outer Join 
The FULL OUTER JOIN keyword return all records when there is a match in either left (table1) or right (table2) table records.
Note: FULL OUTER JOIN can potentially return very large result-sets!

SELECT column_name(s)
FROM table1
FULL OUTER JOIN table2 ON table1.column_name = table2.column_name;

### Except/Minus Operator
SELECT column1 FROM table1
MINUS
SELECT column1 FROM table2

## SQL Lite

Sqlite is very light version of SQL supporting many features of SQL. Basically its been developed for small devices like mobile phones, tablets etc.
SQLite is a third party ,open-sourced and in-process database engine. SQL Server Compact is from Microsoft, and is a stripped-down version of SQL Server.They are two competing database engines.
SQL is query language. Sqlite is embeddable relational database management system.

Small to medium sized applications where your database is just going to live on disk

Once created, .connect just connects to the already created file (no errors)

Creating a cursor means we can’t start running SQL commands using the execute method

SQL has different datatypes to other databases:
NULL - the value is a NULL value
INTEGER - the value is a signed integers, stored in 1, 2, 3, 4, 6, or 8 bytes depending on the magnitude of the value
REAL -  the value is a floating point value, stored as an 8-byte IEEE floating point number
TEXT - the value is a text string, stored using the database encoding (UTF-8, UTF-16BE or UTF - 16LE)
BLOB - the value is a blob of data, stored exactly as it was input

Select statement provides results that can then be iterated through 

fetchone - gets the next down in our result and only returns that row, if there are now rows left it returns none (not as a list)
fetchmany - takes an argument of a number, returns those rows that match the select statement as a list, 
fetchall - doesn’t take any arguments but gets the remaining rows and returns them as a list

Rather than using the execute import statement, if you have variables already set up that you want to load into a database then:
Set a different python module with a class that sets the data into the different columns e.g. first name, last name and pay

Database that lives in ram which is useful for testing 

Setting a database that is equal to memory is useful for testing as it creates a database that lives in ram and creates a fresh clean database every time its run, this means you can test you insert and create statements every time you run the code without causing errors as they haven’t already been created

Can use functions for insertion, updating, finding and deleting the database


Subqueries/Complex Queries

Order of query execution - sequentially from inside out 
Subqueries - 
A subquery is a select-from-where expression that  is nested within another query
A subquery must always appear within parentheses

in - something found in another query
some - atleast one 
any - comparison, return TRUE

Null Values and Three Valued Logic

Scalar subquery - used when a single value is expected


IN and ALL

EXISTS/ NOT EXISTS